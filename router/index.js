const express = require("express");
const { parse } = require("path");
const router = express.Router();
const bodyparser = require("body-parser");

router.get("/", (req, res) => {
    res.render('index.html');

});

router.post("/", (req, res)=>{
    const params={
        Titulo: "Resultados",
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Nivel: req.body.Nivel,
        PagoDiario: req.body.PagoDiario,
        Dias: req.body.Dias,
    };
    res.render("index.html", params);
})

router.get("/resultados", (req, res) => {
    res.render('resultados.html');

});

router.post("/resultados", (req, res)=>{
    const params={
        Titulo: "Resultados",
        Contrato:req.body.NumContrato,
        Nombre: req.body.Nombre,
        Domicilio: req.body.Domicilio,
        Nivel: req.body.Nivel,
        PagoBase: req.body.PagoBase,
        Dias: req.body.Dias,
    };
    res.render("resultados.html", params);
})
module.exports=router;