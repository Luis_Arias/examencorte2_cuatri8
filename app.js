const http = require("http");
const express = require("express");
const app = express();
app.set("view engine", "ejs");
const bodyparser = require("body-parser");
const Rutas = require("./router/index");
const path = require("path");

app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({ extended: true }))
app.use(Rutas);

app.engine("html", require("ejs").renderFile);
const puerto = 2000;
app.listen(puerto, () => {
    console.log("El puerto ha sido iniciado")
});

app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/public/error.html')
})
